//
//  ProfileViewController.swift
//  THoughts
//
//  Created by Carlos Cardona on 21/07/21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    // Profile photo
    
    // Full name
    
    // Email
    
    // List of posts
    private var posts: [BlogPost] = []
    
    private var user: User?
    let currentEmail: String
    
    init(currentEmail: String) {
        self.currentEmail = currentEmail
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setSignOutButton()
        addSubviews()
        setUpTable()
        setUpTableHeader()
        fetchProfileData()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = view.bounds
    }
    
    private func setUpView() {
        view.backgroundColor = .systemBackground
        title = "Profile"
    }
    
    private func addSubviews() {
        view.addSubview(tableView)
    }
    
    private func setUpTable() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func fetchProfileData() {
        
        DatabaseManager.shared.getUser(email: currentEmail) { [weak self] user in
            guard let user = user else {
                return
            }
            
            self?.user = user
            
            DispatchQueue.main.async {
                self?.setUpTableHeader(profilePhotoRef: user.profilePictureRef, name: user.name)
            }
        }
        
    }
    
    private func setUpTableHeader(profilePhotoRef: String? = nil, name: String? = nil) {
        
        let headerVIew = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: view.width / 1.5))
        headerVIew.backgroundColor = .systemBlue
        headerVIew.isUserInteractionEnabled = true
        headerVIew.clipsToBounds = true
        
        tableView.tableHeaderView = headerVIew
        
        // profile picture
        let profilePhoto = UIImageView(image: UIImage(systemName: "person.circle"))
        profilePhoto.tintColor = .white
        profilePhoto.contentMode = .scaleAspectFit
        profilePhoto.frame = CGRect(x: (view.width - (view.width / 4)) / 2,
                                    y: (headerVIew.height - (view.width /  4)) / 2.5,
                                    width: view.width / 4,
                                    height: view.width / 4)
        
        profilePhoto.layer.masksToBounds = true
        profilePhoto.layer.cornerRadius = profilePhoto.width / 2
        profilePhoto.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapProfilePhoto))
        profilePhoto.addGestureRecognizer(tap)
        
        headerVIew.addSubview(profilePhoto)
        
        // Email
        let emailLabel = UILabel(frame: CGRect(x: 20, y: profilePhoto.bottom + 20, width: view.width - 40, height: 100))
        emailLabel.text = currentEmail
        emailLabel.textAlignment = .center
        emailLabel.font = .systemFont(ofSize: 24, weight: .bold)
        headerVIew.addSubview(emailLabel)
        
        if let name = name {
            title = name
        }
        
        if let ref = profilePhotoRef {
            // fetch image
            
            print("Photo ref: \(ref)")
            
            StorageManager.shared.downloadUrlForProfilePicture(path: ref) { url in
                guard let url = url else { return }
                
                let task = URLSession.shared.dataTask(with: url) { data, _, _ in
                    
                    guard let data = data else { return }
                    
                    DispatchQueue.main.async {
                        profilePhoto.image = UIImage(data: data)
                    }
                }
                
                task.resume()
            }
        }
    }
    
    
    
    private func setSignOutButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign Out", style: .done, target: self, action: #selector(didTapSignOut))
    }
    
    // MARK: - Sign Out
    
    @objc private func didTapSignOut() {
        
        let sheet = UIAlertController(title: "Sign Out", message: "Are you sure you want sign out?", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        sheet.addAction(UIAlertAction(title: "Sign Out", style: .destructive, handler: { _ in
            
            AuthManager.shared.signOut { success in
                if success {
                    DispatchQueue.main.async {
                        
                        UserDefaults.standard.set(nil, forKey: "email")
                        UserDefaults.standard.set(nil, forKey: "name")
                        
                        let signinVC = SigninViewController()
                        signinVC.navigationItem.largeTitleDisplayMode = .always
                        
                        let navVC = UINavigationController(rootViewController: signinVC)
                        navVC.navigationBar.prefersLargeTitles = true
                        navVC.modalPresentationStyle = .fullScreen
                        
                        self.present(navVC, animated: true, completion: nil)
                    }
                }
            }
        }))
        present(sheet, animated: true, completion: nil)
    }
    
    @objc private func didTapProfilePhoto() {
        
        guard let myEmail = UserDefaults.standard.string(forKey: "email") else { return }
        
        guard myEmail == currentEmail else { return }
        
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    private func fetchPosts() {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let post = posts[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "Blog post goes here"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = PostViewController()
        vc.title = posts[indexPath.row].title
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else { return }
        
        StorageManager.shared.uploadUserProfilePicture(email: currentEmail,
                                                       image: image) { [weak self] success in
            
            guard let strongSelf = self else { return }
            
            if success {
                // Update database
                DatabaseManager.shared.updateProfilePhoto(email: strongSelf.currentEmail) { updated in
                    
                    guard updated else { return }
                    
                    DispatchQueue.main.async {
                        strongSelf.fetchProfileData()
                    }
                }
            }
        }
    }
    
}

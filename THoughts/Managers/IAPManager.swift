//
//  IAPManager.swift
//  THoughts
//
//  Created by Carlos Cardona on 21/07/21.
//

import Foundation
import Purchases
import StoreKit

final class IAPManager {
    
    static let shared = IAPManager()
    
    private init() { }
    
    func isPremium() -> Bool {
        return UserDefaults.standard.bool(forKey: "premium")
    }
    
    public func getSubscriptionStatus(completion: ((Bool) -> Void)?) {
        Purchases.shared.purchaserInfo { info, error in
            guard let entitlements = info?.entitlements, error == nil else { return }
            
            if entitlements.all["Premium"]?.isActive == true {
                print("Got updated status to subscribed")
                UserDefaults.standard.set(true, forKey: "premium")
                completion?(true)
            } else {
                UserDefaults.standard.set(false, forKey: "premium")
                print("Got updated status to NOT subscribed")
                completion?(false)
            }
        }
    }
    
    public func fetchPackages(completion: @escaping (Purchases.Package?) -> Void) {
        Purchases.shared.offerings { offering, error in
            guard let package = offering?.offering(identifier: "default")?.availablePackages.first, error == nil else {
                completion(nil)
                return
            }
            
            completion(package)
        }
    }
    
    public func subscribe(package: Purchases.Package, completion: @escaping (Bool) -> Void) {
        
        guard !isPremium() else {
            completion(true)
            print("User is already Subscribed")
            return
        }
        
        Purchases.shared.purchasePackage(package) { transaction, info, error, userCancelled in
            guard let transaction = transaction,
                  let entitlements = info?.entitlements,
                  error == nil,
                  !userCancelled else {
                return
            }
            
            switch transaction.transactionState {
            
            case .purchasing:
                print("purchasing")
            case .purchased:
                print("purchased \(entitlements)")
                
                if entitlements.all["Premium"]?.isActive == true {
                    print("Purchased")
                    UserDefaults.standard.set(true, forKey: "premium")
                    completion(true)
                } else {
                    UserDefaults.standard.set(false, forKey: "premium")
                    print("Purchase failed")
                    completion(false)
                }
                
            case .failed:
                print("failed")
            case .restored:
                print("restored")
            case .deferred:
                print("deferred")
            @unknown default:
                print("default case")
            }
        }
    }
    
    public func restorePurchases(completion: @escaping (Bool) -> Void) {
        Purchases.shared.restoreTransactions { info, error in
            guard let entitlements = info?.entitlements,
                  error == nil else {
                return
            }
            
            if entitlements.all["Premium"]?.isActive == true {
                print("Restored Success")
                UserDefaults.standard.set(true, forKey: "premium")
                completion(true)
            } else {
                UserDefaults.standard.set(false, forKey: "premium")
                print("failed to restore")
                completion(false)
            }
        }
    }
}

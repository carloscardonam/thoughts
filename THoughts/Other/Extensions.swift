//
//  Extensions.swift
//  THoughts
//
//  Created by Carlos Cardona on 11/08/21.
//

import Foundation
import UIKit

extension UIView {
    
    public var height: CGFloat {
        return frame.size.height
    }
    
    public var width: CGFloat {
        return frame.size.width
    }
    
    public var top: CGFloat {
        return frame.origin.y
    }
    
    public var bottom: CGFloat {
        return top + height
    }
    
    public var left: CGFloat {
        return frame.origin.x
    }
    
    public var right: CGFloat {
        return left + width
    }
}

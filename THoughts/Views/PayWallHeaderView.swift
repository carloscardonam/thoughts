//
//  PayWallHeaderView.swift
//  THoughts
//
//  Created by Carlos Cardona on 11/08/21.
//

import UIKit

class PayWallHeaderView: UIView {
    
    // Header Image
    private let headerImageView: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 110, height: 110)
        image.image = UIImage(systemName: "crown.fill")
        image.tintColor = .white
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        addSubview(headerImageView)
        backgroundColor = .systemPink
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        headerImageView.frame = CGRect(x: (bounds.width - 110) / 2, y: (bounds.height - 110) / 2, width: 110, height: 110)
    }

}

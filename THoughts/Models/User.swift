//
//  User.swift
//  THoughts
//
//  Created by Carlos Cardona on 09/08/21.
//

import Foundation

struct User {
    let name: String
    let email: String
    let profilePictureRef: String?
}

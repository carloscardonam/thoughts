//
//  BlogPost.swift
//  THoughts
//
//  Created by Carlos Cardona on 09/08/21.
//

import Foundation

struct BlogPost {
    let identifier: String
    let title: String
    let timestamp: TimeInterval
    let headerImageUrl: URL?
    let text: String
}
